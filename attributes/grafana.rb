default['gitlab-monitoring']['grafana']['nginx']['enabled'] = true
default['gitlab-monitoring']['grafana']['nginx']['ssl_enabled'] = true
default['gitlab-monitoring']['grafana']['nginx']['ssl_by_proxy'] = false
default['gitlab-monitoring']['grafana']['nginx']['hostname'] = 'hostname'
default['gitlab-monitoring']['grafana']['nginx']['ssl_key'] = 'ssl_key'
default['gitlab-monitoring']['grafana']['nginx']['ssl_certificate'] = 'ssl_certificate'
default['gitlab-monitoring']['grafana']['nginx']['maintenance_page'] = false

default['gitlab-monitoring']['grafana']['maintenance_page']['title'] = 'Under Maintenance'
default['gitlab-monitoring']['grafana']['maintenance_page']['message'] = 'Please refer to <a href=https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/318>this epic</a> for updates.'

default['gitlab-monitoring']['grafana']['auth.anonymous']['enable'] = false
default['gitlab-monitoring']['grafana']['auth.anonymous']['org_name'] = 'GitLab'
default['gitlab-monitoring']['grafana']['auth.anonymous']['org_role'] = 'Viewer'
default['gitlab-monitoring']['grafana']['users']['allow_sign_up'] = true
default['gitlab-monitoring']['grafana']['users']['disable_login_form'] = false
default['gitlab-monitoring']['grafana']['users']['viewers_can_edit'] = false

default['gitlab-monitoring']['grafana']['server']['root_url'] = 'http://localhost:3000'

default['gitlab-monitoring']['grafana']['auth.generic_oauth']['enabled'] = false
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['name'] = 'OAuth'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['allow_sign_up'] = true
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['client_id'] = 'some_id'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['client_secret'] = 'some_secret'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['scopes'] = 'user:email,read:org'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['auth_url'] = 'https://foo.bar/login/oauth/authorize'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['token_url'] = 'https://foo.bar/login/oauth/access_token'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['api_url'] = 'https://foo.bar/user'
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['team_ids'] = ''
default['gitlab-monitoring']['grafana']['auth.generic_oauth']['allowed_organizations'] = ''

default['gitlab-monitoring']['grafana']['auth.google']['enabled'] = false
default['gitlab-monitoring']['grafana']['auth.google']['allow_sign_up'] = true
default['gitlab-monitoring']['grafana']['auth.google']['client_id'] = 'some_client_id'
default['gitlab-monitoring']['grafana']['auth.google']['client_secret'] = 'some_client_secret'
default['gitlab-monitoring']['grafana']['auth.google']['scopes'] = 'https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/userinfo.email'
default['gitlab-monitoring']['grafana']['auth.google']['auth_url'] = 'https://accounts.google.com/o/oauth2/auth'
default['gitlab-monitoring']['grafana']['auth.google']['token_url'] = 'https://accounts.google.com/o/oauth2/token'
default['gitlab-monitoring']['grafana']['auth.google']['api_url'] = 'https://www.googleapis.com/oauth2/v1/userinfo'
default['gitlab-monitoring']['grafana']['auth.google']['allowed_domains'] = ''

default['gitlab-monitoring']['grafana']['security']['admin_user'] = 'admin'
default['gitlab-monitoring']['grafana']['security']['admin_password'] = 'admin'
default['gitlab-monitoring']['grafana']['security']['disable_gravatar'] = 'true'

default['gitlab-monitoring']['grafana']['version'] = '7.2.0'
