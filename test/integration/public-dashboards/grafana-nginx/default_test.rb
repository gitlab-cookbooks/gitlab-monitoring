# InSpec tests

control 'grafana is running' do
  impact 1.0

  describe service('grafana-server') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end

  describe port(3000) do
    it 'is listening on 3000', retry: 5, retry_wait: 10 do
      should be_listening
    end
  end
end

control 'nginx is running' do
  impact 1.0

  describe service('nginx') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end
