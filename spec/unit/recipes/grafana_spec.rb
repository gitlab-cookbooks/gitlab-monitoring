require 'spec_helper'
require 'chef-vault'

describe 'gitlab-monitoring::grafana' do
  context 'When all nginx is disabled, on an Ubuntu 16.04' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab-monitoring']['grafana']['nginx']['enabled'] = false
      end.converge(described_recipe)
    end
    it 'installs grafana' do
      expect(chef_run).to install_package('grafana')
    end

    it 'does not generate nginx site config' do
      expect(chef_run).to_not create_template('/etc/nginx/sites-available/grafana')
    end
  end

  context 'When all nginx ssl is disabled, on an Ubuntu 16.04' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab-monitoring']['grafana']['nginx']['ssl_enabled'] = false
      end.converge(described_recipe)
    end
    it 'installs grafana' do
      expect(chef_run).to install_package('grafana')
    end
  end

  context 'When all attributes are default, on an Ubuntu 16.04' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        # node.normal['gitlab-monitoring']['chef_vault'] = "gitlab-monitoring"
      end.converge(described_recipe)
    end

    before do
      allow(Chef::DataBag)
        .to receive(:load).with('gitlab-monitoring').and_return(
          '_default_keys' => { 'id' => '_default_keys' },
          '_default' => { 'id' => '_default' }
        )
      allow(ChefVault::Item)
        .to receive(:load).with('gitlab-monitoring', '_default').and_return(
          'id' => '_default',
          'gitlab-monitoring' => {
            'grafana' => {
              'auth.generic_oauth' => {
                'enabled' => true,
                'name' => 'custom_oauth',
              },
            },
          }
        )
    end

    it 'installs grafana' do
      expect(chef_run).to install_package('grafana')
    end

    it 'generates grafana.ini' do
      expect(chef_run).to create_template('/etc/grafana/grafana.ini').with(
        owner: 'root',
        group: 'grafana',
        mode: '0644'
      )
      expect(chef_run).to render_file('/etc/grafana/grafana.ini')
    end

    it 'adds the certificate files' do
      expect(chef_run).to render_file('/etc/ssl/hostname.crt')
      expect(chef_run).to render_file('/etc/ssl/hostname.key')
    end
  end

  context 'When ssl_by_proxy is true' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
        node.normal['gitlab-monitoring']['grafana']['nginx']['ssl_by_proxy'] = true
      end.converge(described_recipe)
    end

    it 'does not add the certificate files' do
      expect(chef_run).to_not render_file('/etc/ssl/hostname.crt')
      expect(chef_run).to_not render_file('/etc/ssl/hostname.key')
    end
  end
end
