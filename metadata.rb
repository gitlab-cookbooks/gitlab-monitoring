name 'gitlab-monitoring'
maintainer 'Marat Kalibekov'
maintainer_email 'marat@gitlab.com'
license 'MIT'
description 'Installs/Configures gitlab-monitoring'
version '1.3.10'
chef_version '>= 12.1'

depends 'nginx'
