## Install nginx
ssl_enabled = node['gitlab-monitoring']['grafana']['nginx']['ssl_enabled']
ssl_by_proxy = node['gitlab-monitoring']['grafana']['nginx']['ssl_by_proxy']
maintenance = node['gitlab-monitoring']['grafana']['nginx']['maintenance_page']

nginx_install 'default' do
  conf_cookbook 'gitlab-monitoring'
  conf_template 'nginx-monitoring.conf.erb'
  source 'repo'
  default_site_enabled false
end

# Workaround nginx_install not exposing the service resource.
service 'nginx' do
  supports status: true, restart: true, reload: true
  action   [:start, :enable]
end

file "/etc/ssl/#{node['gitlab-monitoring']['grafana']['nginx']['hostname']}.crt" do
  content node['gitlab-monitoring']['grafana']['nginx']['ssl_certificate']
  owner 'root'
  group 'root'
  mode '644'
  notifies :restart, 'service[nginx]', :delayed
  only_if { ssl_enabled && !ssl_by_proxy }
end

file "/etc/ssl/#{node['gitlab-monitoring']['grafana']['nginx']['hostname']}.key" do
  content node['gitlab-monitoring']['grafana']['nginx']['ssl_key']
  owner 'root'
  group 'root'
  mode '600'
  notifies :restart, 'service[nginx]', :delayed
  only_if { ssl_enabled && !ssl_by_proxy }
end

nginx_site 'grafana' do
  cookbook 'gitlab-monitoring'
  template ssl_enabled ? 'nginx-site-grafana.erb' : 'nginx-site-grafana-nossl.erb'
  not_if { maintenance }
end

nginx_site 'maintenance' do
  cookbook 'gitlab-monitoring'
  template 'nginx-site-maintenance.erb'
  only_if { maintenance }
end

directory '/var/www' do
  owner 'root'
  group 'root'
  mode '0755'
  action :create
end

file '/var/www/index.html' do
  content "<html><head><title>#{node['gitlab-monitoring']['grafana']['maintenance_page']['title']}</title></head><body><h1>#{node['gitlab-monitoring']['grafana']['maintenance_page']['title']}</h1></br>#{node['gitlab-monitoring']['grafana']['maintenance_page']['message']}</body></html>"
  owner 'root'
  group 'root'
  mode '664'
end
