#
# Cookbook:: gitlab-monitoring
# Recipe:: grafana
#
# Copyright:: 2017, GitLab Inc, All Rights Reserved.

## Add apt repositories
apt_repository 'grafana' do
  uri 'https://packages.grafana.com/oss/deb'
  distribution 'stable'
  components ['main']
  key 'https://packages.grafana.com/gpg.key'
end

# Install grafana
package 'grafana' do
  version node['gitlab-monitoring']['grafana']['version']
end

service 'grafana-server' do
  action :enable
end

template '/etc/grafana/grafana.ini' do
  owner 'root'
  group 'grafana'
  mode '0644'
  notifies :restart, 'service[grafana-server]'
end

include_recipe '::grafana-nginx' if node['gitlab-monitoring']['grafana']['nginx']['enabled']
